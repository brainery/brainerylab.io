FROM alpine:3.6 
MAINTAINER Luis Couto <couto@15minuteslat.net>

ENV HUGO_VERSION 0.29
ENV HUGO_BINARY hugo_${HUGO_VERSION}_linux-64bit
ENV HUGO_FULL_URL https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY}.tar.gz

# Download and Install hugo 
ADD ${HUGO_FULL_URL} /usr/local/bin
